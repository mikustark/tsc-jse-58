package ru.tsc.karbainova.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.endpoint.AdminUserEndpoint;
import ru.tsc.karbainova.tm.endpoint.ProjectEndpoint;
import ru.tsc.karbainova.tm.endpoint.Role;
import ru.tsc.karbainova.tm.endpoint.SessionEndpoint;
import ru.tsc.karbainova.tm.endpoint.TaskEndpoint;
import ru.tsc.karbainova.tm.endpoint.UserEndpoint;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.service.SessionService;


public abstract class AbstractListenerTask extends AbstractSystemListener {

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

}
