package ru.tsc.karbainova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {
    void clear();

    List<Session> findAll();

    List<Session> findAllByUserId(@NotNull String userId);

    Session findById(@Nullable String id);

    void removeById(@Nullable String id);

    void removeByUserId(@NotNull String userId);

    int getCount();
}
