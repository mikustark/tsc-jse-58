package ru.tsc.karbainova.tm.service;

import lombok.Getter;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;
import ru.tsc.karbainova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['jdbc.user']}")
    private String jdbcUser;

    @Value("#{environment['jdbc.password']}")
    private String jdbcPassword;

    @Value("#{environment['jdbc.url']}")
    private String jdbcUrl;

    @Value("#{environment['jdbc.driver']}")
    private String jdbcDriver;

    @Value("#{environment['dialect']}")
    private String dialect;

    @Value("#{environment['auto']}")
    private String auto;

    @Value("#{environment['sqlshow']}")
    private String sqlShow;

    @Value("#{environment['version']}")
    private String applicationVersion;

    @Value("#{environment['developer']}")
    private String developerName;

    @Value("#{environment['developer.email']}")
    private String developerEmail;

    @Value("#{environment['host']}")
    private String serverHost;

    @Value("#{environment['port']}")
    private String serverPort;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['session.iteration']}")
    private Integer signatureIteration;

    @Value("#{environment['session.secret']}")
    private String signatureSecret;

    @Value("#{environment['cache.level']}")
    private String useSecondLevelCache;

    @Value("#{environment['cache.query']}")
    private String useQueryCache;

    @Value("#{environment['cache.minimal-puts']}")
    private String useMinimalPuts;

    @Value("#{environment['cache.prefix']}")
    private String cacheRegionPrefix;

    @Value("#{environment['cache.config']}")
    private String cacheProviderConfig;

    @Value("#{environment['cache.factory']}")
    private String cacheRegionFactory;

}
